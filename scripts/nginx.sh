#!/bin/bash
source scripts/env.sh
mkdir filesystem/etc/nginx/bots-enabled
cat > filesystem/etc/nginx/bots-enabled/$PROJECT_NAME.conf <<EOF
  location /api/v1/$PROJECT_NAME {
    proxy_pass http://$BE_IP:$BE_PORT/api/v1/$PROJECT_NAME;

    access_log /var/log/nginx/$PROJECT_NAME.access.log;
    error_log /var/log/nginx/$PROJECT_NAME.error.log;

  }
EOF
