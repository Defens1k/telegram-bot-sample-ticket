#!/bin/bash
source scripts/env.sh

cat > filesystem/opt/bot/$PROJECT_NAME-start.sh <<EOF
#!/bin/bash
python3 /opt/bot/$PROJECT_NAME.py $BE_PORT
EOF

cat > filesystem/opt/bot/$PROJECT_NAME-stop.sh <<EOF
#!/bin/bash
for i in \$(ps -leax | grep "python3 /opt/bot/$PROJECT_NAME.py" | awk '{print \$3}')
do
  kill -9 \$i
done
EOF

chmod +x filesystem/opt/bot/$PROJECT_NAME-start.sh
chmod +x filesystem/opt/bot/$PROJECT_NAME-stop.sh
